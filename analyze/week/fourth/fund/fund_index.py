import numpy as np
import pandas as pd
from scipy.stats import pearsonr
from sklearn.decomposition import PCA

DATE = 'date'


def replace_date(data):
    data[DATE] = data[DATE].replace('-', '', regex=True)


prices = pd.read_csv('close_prices.csv')
replace_date(prices)
pca = PCA(n_components=10)

pca.fit(prices.drop(DATE, axis=1))


def min_components_count(pca):
    ratio_ = pca.explained_variance_ratio_
    ratio_sum = 0
    for i in range(len(ratio_)):
        ratio_sum += ratio_[i]
        if ratio_sum > 0.9:
            return i + 1


print(min_components_count(pca))
first_component = pca.components_[0]
max_component = 0
component_i = 0
for i in range(1, len(first_component)):
    if max_component < first_component[i]:
        max_component = first_component[i]
        component_i = i
print(component_i)
djia = pd.read_csv('djia_index.csv')
transformed = pca.transform(prices.drop(DATE, axis=1))
first_comp = list()
for i in range(len(transformed)):
    first_comp.append(transformed[i][0])
replace_date(djia)
index = djia['^DJI'].values
corrcoef = np.corrcoef(first_comp, index)
print(corrcoef)
print(round(0.90965222, 2))
