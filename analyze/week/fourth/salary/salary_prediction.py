import pandas as pd
from scipy.sparse import hstack
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import Ridge

MIN_WORD_FREQUENCY = 2

DESCRIPTION_REGEXP = '[^a-zA-Z0-9]'

NAN = 'nan'

DESCRIPTION = 'FullDescription'

LOCATION = 'LocationNormalized'

CONTRACT_TIME = 'ContractTime'

TRAIN = 'salary-train.csv'

salary = pd.read_csv(TRAIN)


def map_to_lower(description):
    return description.lower()


def pre_process(salary_data):
    salary_data[DESCRIPTION] = salary_data[DESCRIPTION].replace(DESCRIPTION_REGEXP, ' ', regex=True).apply(map_to_lower)
    salary_data[LOCATION].fillna(NAN, inplace=True)
    salary_data[CONTRACT_TIME].fillna(NAN, inplace=True)
    return salary_data


train = pre_process(salary_data=salary)
vectorizer = TfidfVectorizer(min_df=MIN_WORD_FREQUENCY)
transformed_desc = vectorizer.fit_transform(train[DESCRIPTION])
dict_vectorizer = DictVectorizer()
transform_location_and_time = dict_vectorizer.fit_transform(X=(train[[LOCATION, CONTRACT_TIME]].to_dict('records')))
stack = hstack((transformed_desc, transform_location_and_time))
ridge = Ridge()
# print(stack)
ridge.fit(stack, salary['SalaryNormalized'])
test = pre_process(pd.read_csv('salary-test-mini.csv'))
X_test = vectorizer.transform(test[DESCRIPTION])
X_test_categ = dict_vectorizer.transform(test[[LOCATION, CONTRACT_TIME]].to_dict('records'))
X_test = hstack((X_test, X_test_categ))
predict = ridge.predict(X_test)
print(predict)
