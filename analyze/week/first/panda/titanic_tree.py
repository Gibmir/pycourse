import pandas as pd
import numpy as np
from pympler import asizeof
from sklearn.tree import DecisionTreeClassifier

NUMERATION_INDEX = 'PassengerId'

TITANIC_DATA_SET = 'titanic.csv'

DELIMITER = ','
# Пример работы
X = np.array([[1, 2], [3, 4], [5, 6]])
y = np.array([0, 1, 0])
clf = DecisionTreeClassifier()
clf.fit(X, y)
importances_ = clf.feature_importances_
print('Важность признаков:' + str(importances_))

data = pd.read_csv(TITANIC_DATA_SET, NUMERATION_INDEX, delimiter=DELIMITER, encoding='ascii')


def map_sex_to_num(sex):
    if sex == 'male':
        return 0
    else:
        return 1


print("Столбцы:" + str(data.keys()))

data_without_nan = data[data['Age'].notnull()]
matrix = data_without_nan[['Pclass', 'Fare', 'Age']].values
matrix_with_sex = np.column_stack([matrix, data_without_nan['Sex'].apply(map_sex_to_num).values])
features = matrix_with_sex
classes = data_without_nan['Survived'].values
print('Features:\n' + str(features))
print('Classes:\n' + str(classes))

classifier = DecisionTreeClassifier(random_state=241)
getsizeof = asizeof.asizeof(classifier)
print(getsizeof)
classifier.fit(features, classes)
feature_importances = classifier.feature_importances_
print(feature_importances)
