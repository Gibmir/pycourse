import pandas as pd

NUMERATION_INDEX = 'PassengerId'

TITANIC_DATA_SET = 'titanic.csv'

DELIMITER = ','


def first_name_func(full_name):
    if 'Miss' in full_name:
        return first_name(miss_full_name(full_name))
    else:
        return first_name(lady_full_name(full_name))


def lady_full_name(full_name):
    return full_name[full_name.find("(") + 1:full_name.find(")")]


def first_name(full_name):
    return full_name.strip().split(' ')[0]


def miss_full_name(full_name):
    miss_symbols = 'Miss. '
    miss_index = full_name.find(miss_symbols)
    return full_name[miss_index+len(miss_symbols):].strip()[:-1]


data = pd.read_csv(TITANIC_DATA_SET, NUMERATION_INDEX, delimiter=DELIMITER, encoding='ascii')
print("Столбцы:" + str(data.keys()))
sex_vector = data['Sex']
# Соотношение мужчин и женщин
print('Соотношение мужчин и женщин:\n' + str(sex_vector.value_counts()))
# Процент выживших
survived_vector = data['Survived']
print(survived_vector.size)
counts = survived_vector.value_counts(normalize=True)[1]
survived_percent = counts * 100
print('Процент выживших:' + str(round(survived_percent, 2)))
# Процент пассажиров первого класса
pclass_vector = data['Pclass']
first_class_passengers = pclass_vector.value_counts(normalize=True)[1]
passengers_percent = first_class_passengers * 100
print('Процент пассажиров первого класса:' + str(round(passengers_percent, 2)))
# Среднее и медиана возраста пассажиров
age_vector = data['Age']
print('Среднее:' + str(round(age_vector.mean(), 2)))
print('Медиана:' + str(round(age_vector.median(), 2)))
# Корреляция между признаками SibSp и Parch
correlationMatrix = data.corr(method='pearson')
parch_sibsp_correlation = correlationMatrix['SibSp']['Parch']
print(str(round(parch_sibsp_correlation, 2)))
# Самое популярное женское имя
names_vector = data[data.Sex == 'female']['Name']
first_name_vector = names_vector.apply(first_name_func)
print(first_name_vector.value_counts())
