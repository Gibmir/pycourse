import numpy as np

print(" Сгенерируйте матрицу, состоящую из 1000 строк и 50 столбцов, "
      "элементы которой являются случайными из нормального распределения N(1,100).")
array_with_random_val = np.random.normal(1, 10, (1000, 50))
print("Array is" + str(array_with_random_val))
print(" Произведите нормировку матрицы из предыдущего задания: вычтите из каждого столбца его среднее значение,"
      " а затем поделите на его стандартное отклонение")
mean = np.mean(array_with_random_val)
std = np.std(array_with_random_val)
array_norm = ((array_with_random_val - mean) / std)
print("Norm array is" + str(array_norm))
print(" Выведите для заданной матрицы номера строк, сумма элементов в которых превосходит 10.")
Z = np.array([[4, 5, 0],
              [1, 9, 3],
              [5, 1, 1],
              [3, 3, 3],
              [9, 9, 9],
              [4, 7, 1]])
np_sum = np.sum(Z, 1)
print(np.nonzero(np_sum > 10))
print("Сгенерируйте две единичные матрицы "
      "(т.е. с единицами на диагонали) размера 3x3. Соедините две матрицы в одну размера 6x3.")
first = np.eye(3)
second = np.eye(3)
stacked = np.vstack((first, second))
print(stacked)
