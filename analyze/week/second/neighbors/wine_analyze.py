import pandas as pd
from sklearn import preprocessing
from sklearn.model_selection import KFold, cross_val_score
from sklearn.neighbors.classification import KNeighborsClassifier

# Оценку качества необходимо провести методом кросс-валидации по 5 блокам (5-fold)
wine_data = pd.read_csv("wine.data", header=None)
classes = wine_data[0].values
print(classes)
features = wine_data.drop(0, axis=1).values
print(features)
k_fold = KFold(n_splits=5, shuffle=True, random_state=42)
neighbors_count_per_score = dict()
for neighbors_count in range(1, 51):
    classifier = KNeighborsClassifier(n_neighbors=neighbors_count)
    classifier.fit(X=features, y=classes)
    score = cross_val_score(estimator=classifier, X=features, y=classes, cv=k_fold)
    neighbors_count_per_score[neighbors_count] = round(number=score.mean(), ndigits=2)
best_neighbor_count = max(neighbors_count_per_score.items(), key=lambda best_score: best_score[1])
print("Best neighbors count and his score:" + str(best_neighbor_count))
scaled_features = preprocessing.scale(features)
neighbors_count_per_score = dict()
for neighbors_count in range(1, 51):
    classifier = KNeighborsClassifier(n_neighbors=neighbors_count)
    classifier.fit(X=scaled_features, y=classes)
    score = cross_val_score(estimator=classifier, X=scaled_features, y=classes, cv=k_fold)
    neighbors_count_per_score[neighbors_count] = round(number=score.mean(), ndigits=2)
best_scaled_neighbor_count = max(neighbors_count_per_score.items(), key=lambda best_score: best_score[1])
print("Best neighbors count and his score after scaling:" + str(best_scaled_neighbor_count))
