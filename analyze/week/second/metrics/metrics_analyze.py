from sklearn import datasets
from sklearn import preprocessing
from sklearn.model_selection import KFold, cross_val_score
from sklearn.neighbors.dist_metrics import DistanceMetric
from sklearn.neighbors.regression import KNeighborsRegressor
import numpy as np

CLASSES_KEY_NAME = 'target'

FEATURES_KEY_NAME = 'data'

boston = datasets.load_boston()
features = boston.get(FEATURES_KEY_NAME)
regress = boston.get(CLASSES_KEY_NAME)
scaled_features = preprocessing.scale(X=features)
params = np.linspace(start=1, stop=10, num=200)
param_per_score = dict()
k_fold = KFold(n_splits=5, shuffle=True, random_state=42)
for param in params:
    regressor = KNeighborsRegressor(n_neighbors=5, weights='distance', p=param)
    regressor.fit(X=scaled_features, y=regress)
    score = cross_val_score(regressor, scaled_features, regress, cv=k_fold, scoring='neg_mean_squared_error')
    param_per_score[param] =score.mean()
best_param = max(param_per_score.items(), key=lambda best_score: best_score[1])
print(param_per_score)
print(best_param)