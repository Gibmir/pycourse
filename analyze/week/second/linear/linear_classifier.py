import pandas as pd
from sklearn.linear_model import Perceptron
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler

train_data = pd.read_csv('perceptron-train.csv', delimiter=',', header=None)
test_data = pd.read_csv('perceptron-test.csv', delimiter=',', header=None)
samples = train_data.drop(0, 1).values
classes = train_data[0].values
test_samples = test_data.drop(0, 1).values
test_classes = test_data[0].values
perceptron = Perceptron(random_state=241, max_iter=5, tol=None)
perceptron.fit(samples, classes)
predicted = perceptron.predict(test_samples)
score = accuracy_score(test_classes, predicted)
print('Accuracy before scaling:' + str(score))
scaler = StandardScaler()
scaled_samples = scaler.fit_transform(samples)
scaled_test_samples = scaler.transform(test_samples)
perceptron.fit(scaled_samples, classes)
scale_predicted = perceptron.predict(scaled_test_samples)
scale_score = accuracy_score(test_classes, scale_predicted)
print('Accuracy after scaling:' + str(scale_score))
result = scale_score - score
print('Result is:' + str(round(result, 3)))
