import pandas as pd
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import r2_score
from sklearn.model_selection import KFold, cross_val_score

RINGS = 'Rings'

N_SPLITS = 5

SEX_F = 'F'

SEX_M = 'M'

SEX = 'Sex'

ABALONE_CSV = 'abalone.csv'

abalone = pd.read_csv(ABALONE_CSV)


def map_sex_to_numeric(data):
    data[SEX] = data[SEX].map(lambda x: 1 if x == SEX_M else (-1 if x == SEX_F else 0))


map_sex_to_numeric(abalone)

x = abalone.drop(RINGS, axis=1)
y = abalone[RINGS]

k_fold = KFold(n_splits=N_SPLITS, shuffle=True, random_state=42)
n_per_score = dict()
for n_estimators in range(1, 51):
    regressor = RandomForestRegressor(n_estimators=n_estimators, random_state=42)
    regressor.fit(x, y)
    score = cross_val_score(estimator=regressor, X=x, y=y, cv=k_fold, scoring='r2')
    n_per_score.update({n_estimators: round(score.mean(), 3)})


def with_score_greater_than(threshold):
    return {k: v for k, v in n_per_score.items() if v > threshold}


result_n = min(with_score_greater_than(0.52).keys())
print(result_n)
