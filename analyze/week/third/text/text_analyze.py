import math

import numpy as np
from sklearn import datasets, svm
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import GridSearchCV, KFold

RANDOM_STATE = 241

ALL_SUBSET = 'all'

SPACE = 'sci.space'

ATHEISM = 'alt.atheism'

news = datasets.fetch_20newsgroups(subset=ALL_SUBSET, categories=[ATHEISM, SPACE])
texts = news.data
classes = news.target
vectorizer = TfidfVectorizer(encoding='utf-8')
X = vectorizer.fit_transform(texts)
features = vectorizer.get_feature_names()
print("Features is:" + str(features))
grid = {'C': np.power(10.0, np.arange(-5, 6))}
k_fold = KFold(n_splits=5, shuffle=True, random_state=RANDOM_STATE)
classifier = svm.SVC(kernel='linear', random_state=RANDOM_STATE, C=1.0)
grid_search = GridSearchCV(classifier, grid, scoring='accuracy', cv=k_fold)
print(X.getnnz())
print(len(classes))
classifier.fit(X=X, y=classes)
coef_ = classifier.coef_
print(coef_.argmax())

coef_list = list(coef_.getrow(0).toarray()[0])
coef_list = list(map(lambda coef: math.fabs(coef), coef_list))
coef_sorted = sorted(coef_list, reverse=True)
best_coef = coef_sorted[:10]
bestNames = list()
for i in range(len(coef_list)):
    if coef_list[i] in best_coef:
        bestNames.append(features[i])

print(best_coef)
print(bestNames)
# grid_search.fit(X=X, y=classes)
# results_ = grid_search.cv_results_
# print(results_)
# найти лучшие 10
# {      'mean_fit_time': array([2.88416495, 2.96556954, 3.03257341, 2.9775703 , 2.4633409 ,
#        1.5614893 , 1.5182869 , 1.47728457, 1.47128425, 1.47988467,
#        1.49168534]),
#        'std_fit_time': array([0.06404113, 0.16499253, 0.14835891, 0.0920587 , 0.04203759,
#        0.06858547, 0.03992424, 0.02367741, 0.02064485, 0.02186819,
#        0.04229934]),
#        'mean_score_time': array([0.73324199, 0.7178411 , 0.7076406 , 0.69924006, 0.58403335,
#        0.34962001, 0.34201941, 0.34081945, 0.33801923, 0.34341974,
#        0.34241953]),
#        'std_score_time': array([0.02319968, 0.03005769, 0.02114453, 0.01791645, 0.0092742 ,
#        0.01906516, 0.01091848, 0.00854212, 0.00805026, 0.01059488,
#        0.00939428]),
#        'param_C': masked_array(data=[1e-05, 0.0001, 0.001, 0.01, 0.1, 1.0, 10.0, 100.0,
# #                    1000.0, 10000.0, 100000.0],
#              mask=[False, False, False, False, False, False, False, False,
#                    False, False, False],
#        fill_value='?',
#             dtype=object),
#        'params': [{'C': 1e-05}, {'C': 0.0001}, {'C': 0.001},
#        {'C': 0.01}, {'C': 0.1}, {'C': 1.0}, {'C': 10.0}, {'C': 100.0},
#        {'C': 1000.0}, {'C': 10000.0}, {'C': 100000.0}],
#        'split0_test_score': array([0.54469274, 0.54469274, 0.54469274, 0.54469274, 0.95810056,
#        0.99441341, 0.99441341, 0.99441341, 0.99441341, 0.99441341,
#        0.99441341]),
#        'split1_test_score': array([0.57983193, 0.57983193, 0.57983193, 0.57983193, 0.94957983,
#        0.9859944 , 0.9859944 , 0.9859944 , 0.9859944 , 0.9859944 ,
#        0.9859944 ]),
#        'split2_test_score': array([0.57142857, 0.57142857, 0.57142857, 0.57142857, 0.95798319,
#        1.        , 1.        , 1.        , 1.        , 1.        ,
#        1.        ]),
#        'split3_test_score': array([0.50140056, 0.50140056, 0.50140056, 0.50140056, 0.93557423,
#        0.99159664, 0.99159664, 0.99159664, 0.99159664, 0.99159664,
#        0.99159664]),
#        'split4_test_score': array([0.56582633, 0.56582633, 0.56582633, 0.56582633, 0.94957983,
#        0.99439776, 0.99439776, 0.99439776, 0.99439776, 0.99439776,
#        0.99439776]),
#        'mean_test_score': array([0.55263158, 0.55263158, 0.55263158, 0.55263158, 0.95016797,
#        0.99328108, 0.99328108, 0.99328108, 0.99328108, 0.99328108,
#        0.99328108]),
#        'std_test_score': array([0.02811723, 0.02811723, 0.02811723, 0.02811723, 0.00821778,
#        0.00455086, 0.00455086, 0.00455086, 0.00455086, 0.00455086,
#        0.00455086]),
#        'rank_test_score': array([8, 8, 8, 8, 7, 1, 1, 1, 1, 1, 1])}
print("Optimal C:" + str(1.0))
print('Optimal test score:' + str(0.99328108))
