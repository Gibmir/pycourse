import pandas as pd
from sklearn import svm

data = pd.read_csv('svm-data.csv', delimiter=',', header=None)
classes = data[0].values
samples = data.drop(0, 1).values
classifier = svm.SVC(C=100_000, kernel='linear', random_state=241)
classifier.fit(samples, classes)
support_objects = classifier.support_
print(support_objects)
