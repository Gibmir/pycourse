import pandas as pd
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, roc_auc_score, \
    precision_recall_curve

classification_data = pd.read_csv('classification.csv')
true_classes = classification_data['true']
predicted_classes = classification_data['pred']
tp = 0
fp = 0
fn = 0
tn = 0
for i in range(len(true_classes)):
    true_class = true_classes[i]
    predicted_class = predicted_classes[i]
    if true_class == predicted_class:
        if true_class == 1:
            tp += 1
        else:
            tn += 1
    else:
        if true_class == 0:
            fp += 1
        else:
            fn += 1
print('True positive is:' + str(tp))
print('False positive is:' + str(fp))
print('False negative is:' + str(fn))
print('True negative is:' + str(tn))

accuracy = round(accuracy_score(true_classes, predicted_classes), 2)
print('Accuracy is:' + str(accuracy))
precision = round(precision_score(true_classes, predicted_classes), 2)
print('Precision is:' + str(precision))
recall = round(recall_score(true_classes, predicted_classes), 2)
print('Recall is:' + str(recall))
f = round(f1_score(true_classes, predicted_classes), 2)
print('F is:' + str(f))

classifiers_scores = pd.read_csv('scores.csv')
true_score = classifiers_scores['true']
logreg_score = classifiers_scores['score_logreg']
svm_score = classifiers_scores['score_svm']
knn_score = classifiers_scores['score_knn']
tree_score = classifiers_scores['score_tree']
roc_logreg = roc_auc_score(true_score, logreg_score)
roc_svm = roc_auc_score(true_score, svm_score)
roc_knn = roc_auc_score(true_score, knn_score)
roc_tree = roc_auc_score(true_score, tree_score)
print('logreg:' + str(roc_logreg))
print('svm:' + str(roc_svm))
print('knn:' + str(roc_knn))
print('tree:' + str(roc_tree))


def find_best_precision(precisions, recalls):
    good_precisions = list()
    for i in range(len(recalls)):
        if recalls[i] >= 0.7:
            good_precisions.append(precisions[i])
    return max(good_precisions)


p_logreg, r_logreg, c_logreg = precision_recall_curve(true_score, logreg_score)
p_svm, r_svm, c_svm = precision_recall_curve(true_score, svm_score)
p_knn, r_knn, c_knn = precision_recall_curve(true_score, knn_score)
p_tree, r_tree, c_tree = precision_recall_curve(true_score, tree_score)

print(round(find_best_precision(p_logreg, r_logreg), 2))
print(round(find_best_precision(p_svm, r_svm), 2))
print(round(find_best_precision(p_knn, r_knn), 2))
print(round(find_best_precision(p_tree, r_tree), 2))
